require('./env.js');
gulp = require('gulp');
less = require('gulp-less');
path = require('path');
LessAutoprefix = require('less-plugin-autoprefix');
autoprefix = new LessAutoprefix({ browsers: ['last 2 versions'] });
cleanCSS = require('gulp-clean-css');
//var livereload = require('gulp-livereload');
watch = require('gulp-watch');
//var watchLess = require('gulp-watch-less');
concat = require('gulp-concat'); // Склейка файлов
browserSync = require('browser-sync').create();
debug = require('gulp-debug');
csso = require('gulp-csso');
stripCssComments = require('gulp-strip-css-comments');
sourceMaps = require('gulp-sourcemaps');
uglify = require('gulp-uglify');
deleteFile = require('gulp-delete-file');


gulp.task('default', ['watch']);

gulp.task('less', function () {
    return gulp.src('./wordpress/wp-content/themes/skyprof/assets/less/init.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ],
            plugins: [autoprefix]
        }))
        .pipe(debug({title: 'css optimized by csso:'}))
        .pipe(csso())
        .pipe(debug({title: 'css comments was removed:'}))
        .pipe(stripCssComments())
        .pipe(debug({title: 'css file was cleaned:'}))
        .pipe(cleanCSS({compatibility: 'ie9'}))
        .pipe(debug({title: 'concat output css file:'}))
        .pipe(concat('style.css'))
        .pipe(debug({title: 'dest output file:'}))
        .pipe(gulp.dest('./wordpress/wp-content/themes/skyprof/'))
        .pipe(browserSync.stream());
});

gulp.task('copy-src', function () {
    return gulp.src([
            // './node_modules/bootstrap/dist/js/bootstrap.js',
            // './node_modules/bootstrap/js/transition.js',
            // './node_modules/bootstrap-material-design/dist/js/material.js',
            // './node_modules/bootstrap-material-design/dist/js/ripples.js',
            // './node_modules/uikit/dist/js/uikit.min.js',
            './node_modules/bootstrap/less/**',
            './node_modules/bootstrap-material-design/less/**',
            './node_modules/swiper/dist/css/swiper.min.css',
            './node_modules/swiper/dist/js/swiper.jquery.min.js'
        ])
        .pipe(debug({title: 'copy file:'}))
        .pipe(gulp.dest('./wordpress/wp-content/themes/skyprof/assets/node-modules-src'));
});

gulp.task('js:develop', function() {
    return gulp.src([
            // './node_modules/uikit/dist/js/uikit-icons.min.js',
            './wordpress/wp-content/themes/skyprof/assets/vendors/bootstrap/dist/js/bootstrap.min.js',
            './wordpress/wp-content/themes/skyprof/assets/vendors/bootstrap-material-design/dist/js/material.js',
            './wordpress/wp-content/themes/skyprof/assets/vendors/bootstrap-material-design/dist/js/ripples.js',
            './wordpress/wp-content/themes/skyprof/assets/js/*.js'
        ])
        .pipe(debug({title: 'concat js:'}))
        .pipe(concat('index.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(debug({title: 'dest js:'}))
        .pipe(gulp.dest('./wordpress/wp-content/themes/skyprof/js'))
        // .pipe(livereload('./')); // даем команду на перезагрузку страницы
});

gulp.task('js:production-debug', function() {
    return gulp.src([
        // './node_modules/uikit/dist/js/uikit-icons.min.js',
        './wordpress/wp-content/themes/skyprof/assets/vendors/bootstrap/dist/js/bootstrap.min.js',
        './wordpress/wp-content/themes/skyprof/assets/vendors/bootstrap-material-design/dist/js/material.js',
        './wordpress/wp-content/themes/skyprof/assets/vendors/bootstrap-material-design/dist/js/ripples.js',
        './wordpress/wp-content/themes/skyprof/assets/js/*.js'
    ])
        .pipe(debug({title: 'init source maps:'}))
        .pipe(sourceMaps.init())
        .pipe(debug({title: 'minify js:'}))
        .pipe(uglify())
        .pipe(debug({title: 'concat js:'}))
        .pipe(concat('index.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(debug({title: 'write source maps:'}))
        .pipe(sourceMaps.write('../js'))
        .pipe(debug({title: 'dest js:'}))
        .pipe(gulp.dest('./wordpress/wp-content/themes/skyprof/js'))
    // .pipe(livereload('./')); // даем команду на перезагрузку страницы
});

gulp.task('js:production', ['js:delete-sourcemaps', 'js:production-cleaned']);

gulp.task('js:production-cleaned', function() {
    return gulp.src([
        // './node_modules/uikit/dist/js/uikit-icons.min.js',
        './wordpress/wp-content/themes/skyprof/assets/vendors/bootstrap/dist/js/bootstrap.min.js',
        './wordpress/wp-content/themes/skyprof/assets/vendors/bootstrap-material-design/dist/js/material.js',
        './wordpress/wp-content/themes/skyprof/assets/vendors/bootstrap-material-design/dist/js/ripples.js',
        './wordpress/wp-content/themes/skyprof/assets/js/*.js'
    ])
        .pipe(debug({title: 'minify js:'}))
        .pipe(uglify())
        .pipe(debug({title: 'concat js:'}))
        .pipe(concat('index.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(debug({title: 'dest js:'}))
        .pipe(gulp.dest('./wordpress/wp-content/themes/skyprof/js'))
    // .pipe(livereload('./')); // даем команду на перезагрузку страницы
});

gulp.task('js:delete-sourcemaps', function() {
    var regexp = /.*\.map/;
    return gulp.src([
        // './node_modules/uikit/dist/js/uikit-icons.min.js',
        './wordpress/wp-content/themes/skyprof/assets/js/*.map',
        './wordpress/wp-content/themes/skyprof/js/*.map'
    ])
        .pipe(debug({title: 'remove .map files:'}))
        .pipe(deleteFile({
            reg: regexp,
            deleteMatch: true
        }))
    // .pipe(livereload('./')); // даем команду на перезагрузку страницы
});

gulp.task('new_less', function(){
    return gulp.src('./wordpress/wp-content/themes/skyprof/assets/new_less/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('./wordpress/wp-content/themes/skyprof/css'));
});

gulp.task('templates', function() {
   return browserSync.reload();
});

gulp.task('watch', function() {
    browserSync.init({
        proxy: proxy,
        host: host,
        open: "external",
        // https: http,
        notify: true,
        port: 8000
    });
    // livereload.listen();
    gulp.watch('./wordpress/wp-content/themes/skyprof/assets/less/**/*.less', ['less']);
    gulp.watch('./wordpress/wp-content/themes/skyprof/assets/js/**/*.js', ['js:develop']);
    gulp.watch('./wordpress/wp-content/themes/skyprof/**/*.php', ['templates']);
});